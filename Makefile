SERVERS_DATA := $(CURDIR)/data/generated/servers.json
TFSTATE_BACKEND_PORT := 8888
TFSTATE_REPO_PATH := $(CURDIR)/.tfstate-repo

## Hetzner

.PHONY: hetzner-init
hetzner-init: hetzner/backend.tfvars
	cd hetzner && terraform init -backend-config "$(CURDIR)/hetzner/backend.tfvars"

.PHONY: hetzner-output
hetzner-output: ensure-hetzner-applied
	cd hetzner && terraform output -json servers > $(SERVERS_DATA)

.PHONY: hetzner
hetzner: hetzner-apply

.PHONY: hetzner-%
hetzner-%:
	cd hetzner && terraform $(*)

.PHONY: ensure-hetzner-applied
ensure-hetzner-applied:
	cd hetzner && terraform plan -detailed-exitcode

## Web

.PHONY: web-init
web-init: web/backend.tfvars
	cd web && terraform init -backend-config "$(CURDIR)/web/backend.tfvars"

.PHONY: web-apply
web-apply:
	jq -f utils/web-vars.jq $(SERVERS_DATA) > web/main.tfvars.json
	cd web && terraform apply -var-file=main.tfvars.json -auto-approve

.PHONY: web-%
web-%:
	jq -f utils/web-vars.jq $(SERVERS_DATA) > web/main.tfvars.json
	cd web && terraform $(*) -var-file=main.tfvars.json

.PHONY: web
web: web-apply

## Workers

.PHONY: workers-init
workers-init: workers/backend.tfvars
	cd workers && terraform init -backend-config "$(CURDIR)/workers/backend.tfvars"

.PHONY: workers-apply
workers-apply:
	jq -f utils/workers-vars.jq $(SERVERS_DATA) > workers/main.tfvars.json
	cd workers && terraform apply -var-file=main.tfvars.json -auto-approve

.PHONY: workers-%
workers-%:
	jq -f utils/workers-vars.jq $(SERVERS_DATA) > workers/main.tfvars.json
	cd workers && terraform $(*) -var-file=main.tfvars.json

.PHONY: workers
workers: workers-apply

## Operations

ssh-key.dec:
	pass operator-ssh >! ssh-key.dec && chmod 0600 ssh-key.dec

.PHONY: ssh-%
ssh-%: ssh-key.dec
	ssh root@$(shell jq '."$(*)".ipv6_address' $(SERVERS_DATA) ) -i ssh-key.dec

ssh-run-%: ssh-key.dec
	ssh -q root@$(shell jq '."$(*)".ipv6_address' $(SERVERS_DATA) ) -i ssh-key.dec $(SSH_CMD)

.PHONY: ssh4-%
ssh4-%: ssh-key.dec
	ssh root@$(shell jq '."$(*)".ipv4_address' $(SERVERS_DATA) ) -i ssh-key.dec

ssh4-run-%: ssh-key.dec
	ssh -q root@$(shell jq '."$(*)".ipv4_address' $(SERVERS_DATA) ) -i ssh-key.dec $(SSH_CMD)

.PHONY: ssh-worker-%
ssh-worker-%: ssh-key.dec
	ssh root@$(shell jq '.workers."$(*)".ipv6_address' $(SERVERS_DATA) ) -i ssh-key.dec

.PHONY: ssh4-worker-%
ssh4-worker-%: ssh-key.dec
	ssh root@$(shell jq '.workers."$(*)".ipv4_address' $(SERVERS_DATA) ) -i ssh-key.dec

.PHONY: reset-vault
reset-vault:
	scripts/reset-vault.sh
	scripts/load-vault.sh

## State repository

$(TFSTATE_REPO_PATH):
	git clone git@git.coop:akshay/concourse.gdn.tfstate.git $(TFSTATE_REPO_PATH)

.PHONY: reset-tfstate-repo
reset-tfstate-repo: delete-tf-state-repo $(TFSTATE_REPO_PATH)

.PHONY: delete-tfstate-repo
delete-tf-state-repo:
	rm -rf $(TFSTATE_REPO_PATH)

## State backend

.PHONY: run-tfstate-backend
run-tfstate-backend:
	terraform-http-backend-pass --port "$(TFSTATE_BACKEND_PORT)" --repositoryPath "$(TFSTATE_REPO_PATH)"

.PHONY: start-tfstate-backend
start-tfstate-backend: $(TFSTATE_REPO_PATH)
	nohup terraform-http-backend-pass --port "$(TFSTATE_BACKEND_PORT)" --repositoryPath "$(TFSTATE_REPO_PATH)" 2>&1 > tfstate-backend.log & echo $$! > tfstate-backend.pid

.PHONY: stop-tfstate-backend
stop-tfstate-backend:
	kill $(shell cat tfstate-backend.pid) && rm tfstate-backend.pid

.PHONY: force-stop-tfstate-backend
force-stop-tfstate-backend:
	kill -9 $(shell cat tfstate-backend.pid) && rm tfstate-backend.pid

## Terraform backend vars

.PHONY: %/backend.tfvars
%/backend.tfvars:
	echo 'address = "http://localhost:$(TFSTATE_BACKEND_PORT)/state/$(*)"' > "$(CURDIR)/$(*)/backend.tfvars"
