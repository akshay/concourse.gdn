# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/camptocamp/pass" {
  version = "2.0.0"
  hashes = [
    "h1:sQ5IlQHIMGMR7jqrrKkkoncnkZvpZp08qFRqXYzkv8E=",
    "zh:04bda4bdfda18c4974e95eb4802e0a9bfb351f1cc932b9d8bc6f86c2bb267e3b",
    "zh:0e7ce73fe3c8c346d4cb2906025647cb9b03c09baf825e97d82cee923e887092",
    "zh:52e11c8c213e716a81b7baf5fcad158a1e97206de65d2ba139f8240a00237b8c",
    "zh:774aaca25d279327329c0551ad466cb5a304815ea8e8e5546eb57de746a74998",
    "zh:a1c5597adebf8b6a07d9a9a7c1c46b11ce7d814eacfabbf9e2fdf1ebd996b377",
    "zh:d4d0060b78e834f7d9bf5d8c6c35bfe2cd564edce42c19dac82667acfa0ca048",
    "zh:fa11074bab6f884f912a24b53126d465c16adb8abd41652449fea868136ea18b",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version = "1.24.1"
  hashes = [
    "h1:TEvQLx8GeL3cYf69r4C3zS96QQusHAMuXN1yDeFANs4=",
    "zh:28c586346c1b839c0ed0cac0bd4d233be0f518028f958c54974975a6f6bf60b0",
    "zh:29d4c0d3c865746286bdaf762344d9d199fe596c8bb535198c968994dc53adb8",
    "zh:408450425943859e04a77da3e111b980ef951644d193e9821c1a52e9399722dd",
    "zh:5455ed4d757c726ac489516f23608ed8fa196e2bf75b767b5bf9e290f70228e9",
    "zh:5eb35abdb6d4a3b0f4f0d43042064183105d12532757eefa46bb50c280ed1933",
    "zh:605f6e97ee8831a71d557ca5193e4fd9ed80bd91b127a2b63eb4e7ce843b239c",
    "zh:9fe9ab6f3d050ae12872fbe52c9328360530d74db3c47a068050e4a498ba5c18",
    "zh:d2641343c287b64c5edb797c4dd2685805760288e7f15c23a794e691d7ffd717",
    "zh:d39b68a509ecd80b2a5204b0dec68327cfac184db6f3ebdfad39b9ce3f842b05",
    "zh:e47bbb42bbcb6489d0bab2c007e80fa9a948155eade0308f0d524e5a73cd6f4a",
    "zh:ffe3a9fe983afe3aa3f8f38d1d600b71f8cdd850f85370cf66ee07bdf17f954f",
  ]
}

provider "registry.terraform.io/psychopenguin/gandi" {
  version     = "2.0.0-rc3"
  constraints = "2.0.0-rc3"
  hashes = [
    "h1:UtxuqLUMxLKprC9ZkHcFQHq/yeeKCAFrgydPb0Tadoo=",
    "zh:192367577f0a33a9ab063bab3858fcb78a3f9c7ef59a9bd49cfccd9d994f2439",
    "zh:24f260d0263d14da52da816d54945d317f07dd912960c9306cec466430014079",
    "zh:35a32a97484a4be5cc395db54751b6589da3e9f8d14ac6459d5d89d71735a577",
    "zh:4cbbd33df6aefc5a499f762645e26e09b7255b7d00a220233eb0bd1abee37942",
    "zh:9073e15d52eefc0d29123272b037a6d6d3ab36857bf28aebab75cf9527fb7055",
    "zh:a51359cb79a12a99666436f3d4e4e30045d4690d6fac02885c703df40ed38813",
    "zh:c22f625d89b6894164097cabd25a40038ed51ac07e7b1288816919e0f77fabf6",
    "zh:f50d81dbd7f4e0247385eb43ff680407918f6090bfb634ec255bae194ac52e88",
  ]
}

provider "registry.terraform.io/terraform-providers/hcloud" {
  version = "1.24.1"
  hashes = [
    "h1:TEvQLx8GeL3cYf69r4C3zS96QQusHAMuXN1yDeFANs4=",
    "zh:28c586346c1b839c0ed0cac0bd4d233be0f518028f958c54974975a6f6bf60b0",
    "zh:29d4c0d3c865746286bdaf762344d9d199fe596c8bb535198c968994dc53adb8",
    "zh:408450425943859e04a77da3e111b980ef951644d193e9821c1a52e9399722dd",
    "zh:5455ed4d757c726ac489516f23608ed8fa196e2bf75b767b5bf9e290f70228e9",
    "zh:5eb35abdb6d4a3b0f4f0d43042064183105d12532757eefa46bb50c280ed1933",
    "zh:605f6e97ee8831a71d557ca5193e4fd9ed80bd91b127a2b63eb4e7ce843b239c",
    "zh:9fe9ab6f3d050ae12872fbe52c9328360530d74db3c47a068050e4a498ba5c18",
    "zh:d2641343c287b64c5edb797c4dd2685805760288e7f15c23a794e691d7ffd717",
    "zh:d39b68a509ecd80b2a5204b0dec68327cfac184db6f3ebdfad39b9ce3f842b05",
    "zh:e47bbb42bbcb6489d0bab2c007e80fa9a948155eade0308f0d524e5a73cd6f4a",
    "zh:ffe3a9fe983afe3aa3f8f38d1d600b71f8cdd850f85370cf66ee07bdf17f954f",
  ]
}
