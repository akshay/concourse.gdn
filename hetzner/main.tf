provider "pass" {}

data "pass_password" "hcloud-token" {
  path = "hcloud-token"
}

data "pass_password" "gandi-api-key" {
  path = "gandi-api-key"
}

provider "hcloud" {
  token = data.pass_password.hcloud-token.password
}

provider "gandi" {
  key = data.pass_password.gandi-api-key.password
}

resource "hcloud_ssh_key" "operator" {
  name       = "operator"
  public_key = file("../data/public-keys/operator.pem")
}

#######
# ATC #
#######

resource "hcloud_server" "concourse-web" {
  name        = "concourse-web"
  server_type = "cx11"
  image       = "ubuntu-20.04"
  ssh_keys    = [hcloud_ssh_key.operator.name]
  user_data   = <<EOF
#cloud-config

runcmd:
- curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-20.09 bash 2>&1 | tee /tmp/infect.log
EOF
}

resource "hcloud_volume" "concourse-web-data" {
  name      = "concourse-web-data"
  size      = 10
  server_id = hcloud_server.concourse-web.id
  format    = "ext4"
}

data "gandi_livedns_domain" "concourse-gdn" {
  name = "concourse.gdn"
}

resource "gandi_livedns_record" "ci-concourse-gdn-A" {
  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = "@"
  type   = "A"
  ttl    = 300
  values = [hcloud_server.concourse-web.ipv4_address]
}

resource "gandi_livedns_record" "ci-concourse-gdn-AAAA" {
  zone   = data.gandi_livedns_domain.concourse-gdn.id
  name   = "@"
  type   = "AAAA"
  ttl    = 300
  values = [hcloud_server.concourse-web.ipv6_address]
}

###########
# WORKERS #
###########

resource "hcloud_server" "workers" {
  for_each = {
    worker-1 = { type = "cx21" }
  }

  name        = each.key
  server_type = each.value.type
  image       = "ubuntu-20.04"
  ssh_keys    = [hcloud_ssh_key.operator.name]
  user_data   = <<EOF
#cloud-config

runcmd:
- curl https://raw.githubusercontent.com/elitak/nixos-infect/master/nixos-infect | PROVIDER=hetznercloud NIX_CHANNEL=nixos-20.09 bash 2>&1 | tee /tmp/infect.log
EOF
}

output "servers" {
  value = {
    web = {
      ipv4_address = hcloud_server.concourse-web.ipv4_address
      ipv6_address = hcloud_server.concourse-web.ipv6_address
      data_device  = hcloud_volume.concourse-web-data.linux_device
    },
    workers = { for name, server in hcloud_server.workers :
      name => {
        ipv4_address = server.ipv4_address
        ipv6_address = server.ipv6_address
      }
    }
  }
}
