terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
    pass = {
      source = "camptocamp/pass"
    }
    gandi = {
      version = "2.0.0-rc3"
      source   = "psychopenguin/gandi"
    }
  }
  required_version = ">= 0.14"
}
