{ stdenv, fetchurl, autoPatchelfHook }:
stdenv.mkDerivation rec {
  pname = "concourse";
  version = "7.7.1";

  src = fetchurl {
    url = "https://github.com/concourse/concourse/releases/download/v${version}/concourse-${version}-linux-amd64.tgz";
    sha256 = "sha256:1n5yvl61z863mr6p92hcsijmd82f599xllvzhhzhz33s3sg9rhzr";
  };

  nativeBuildInputs = [ autoPatchelfHook ];

  installPhase = ''
    mkdir -p $out/bin $out/fly-assets $out/resource-types
    cp -r bin/* $out/bin
    cp -r fly-assets/* $out/fly-assets
    cp -r resource-types/* $out/resource-types
  '';
}
