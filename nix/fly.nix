{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  pname = "fly";
  version = "7.6.0";

  src = fetchurl {
    url = "https://github.com/concourse/concourse/releases/download/v${version}/fly-${version}-linux-amd64.tgz";
    sha256 = "sha256-wmTZy5edBVmOROBSciDiHi8gVk9jzBH5jISAdomXQz8=";
  };

  unpackPhase = ''
    mkdir -p $out/bin
    tar xf $src -C $out/bin
  '';

  dontInstall = true;
}
