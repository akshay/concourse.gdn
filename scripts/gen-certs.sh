#!/usr/bin/env bash

set -euo pipefail

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" &> /dev/null && pwd )"

ca=$(cfssl genkey -initca "$ROOT_DIR/certs/vault/ca.json")

ca_cert=$(jq -r ".cert" <<< "$ca")
ca_key=$(jq -r ".key" <<< "$ca")

echo "$ca_cert" \
    | pass insert -m "vault/ca-cert"
echo "$ca_key" \
    | pass insert -m "vault/ca-key"

vault=$(cfssl gencert -ca <(echo "$ca_cert") -ca-key <(echo "$ca_key") "$ROOT_DIR/certs/vault/server.json")

jq -r ".cert" <<< "$vault" \
    | pass insert -m "vault/server-cert"
jq -r ".key" <<< "$vault" \
    | pass insert -m "vault/server-cert-key"

concourse=$(cfssl gencert -ca <(echo "$ca_cert") -ca-key <(echo "$ca_key") "$ROOT_DIR/certs/vault/concourse-client.json")

jq -r ".cert" <<< "$concourse" \
    | pass insert -m "vault/concourse-client-cert"
jq -r ".key" <<< "$concourse" \
    | pass insert -m "vault/concourse-client-cert-key"

echo "Certificates generated, run 'make web' to deploy the new certificates."
