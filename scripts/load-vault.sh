#!/usr/bin/env bash

set -euo pipefail

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" &> /dev/null && pwd )"

function run-on-web() {
    make -s -C "$ROOT_DIR" ssh4-run-web SSH_CMD="$1"
}

rootToken=$(pass vault/initial-root-token)

while IFS= read -r f; do
    secretPath=$(realpath --relative-to="$ROOT_DIR/secrets/pipeline" "$f" | sed 's/\.gpg$//')
    echo "Creating '$secretPath'"
    pass "pipeline/$secretPath" \
        | sed -z '$ s/\n$//' \
        | run-on-web "VAULT_TOKEN=$rootToken VAULT_CACERT=/var/keys/vault-ca vault kv put concourse/$secretPath value=-"
done < <(find "$ROOT_DIR/secrets/pipeline" -type f)
