#!/usr/bin/env bash

set -euo pipefail

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" &> /dev/null && pwd )"

function run-on-web() {
    make -s -C "$ROOT_DIR" ssh4-run-web SSH_CMD="$1"
}

echo "Resetting vault"
initOut=$(make -s -C "$ROOT_DIR" ssh4-run-web SSH_CMD="reset-vault")

echo "Saving creds"
unsealKey=$(jq -r '.unseal_keys_b64[0]'<<< "$initOut" \
    | base64 -d \
    | gpg --decrypt)

echo "$unsealKey" | pass insert -m "vault/unseal-key"

rootToken=$(jq -r '.root_token' <<< "$initOut")
echo "$rootToken" | pass insert -m "vault/initial-root-token"

echo "Unsealing vault"

run-on-web "vault operator unseal -ca-cert /var/keys/vault-ca '$unsealKey'"
run-on-web "VAULT_TOKEN=$rootToken setup-concourse-access"

echo "Restarting Concourse"
run-on-web "systemctl restart concourse-web"
