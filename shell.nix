let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
  pkgs-20-09 = import sources.nixos-20-09 {};
  terraform-http-backend-pass = import sources.terraform-http-backend-pass {
    inherit pkgs;
    compiler = "ghc8104";
  };
  fly = pkgs.callPackage ./nix/fly.nix {};

  nixpath = pkgs.writeTextFile {
    name = "nixpath-20.09";
    destination = "/nixpath-20.09";
    text = toString pkgs-20-09.path;
  };
in {
  env = pkgs.buildEnv {
    name = "concourse.gdn";
    paths = with pkgs; [
      pass
      niv
      gnumake
      terraform_0_14
      jq
      terraform-ls
      terraform-http-backend-pass
      fly
      shellcheck
      cfssl
      nixpath
    ];
  };
}
