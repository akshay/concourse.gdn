{
  ipv4: .web.ipv4_address,
  ipv6: .web.ipv6_address,
  "data-device": .web.data_device,
  workers: .workers | keys
}