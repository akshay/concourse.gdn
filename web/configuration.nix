{ pkgs, config, ipv6, ipv6-prefix-length, operator-public-key, data-device, ... }:
let
  concourse = pkgs.callPackage ../nix/concourse.nix {};
  data-mount-point = "/concourse-data";
  data-dir = "${data-mount-point}/data";
  vault-migrations = pkgs.callPackage ./vault-migrations/default.nix {};
  vault-scripts = pkgs.callPackage ./vault-scripts/default.nix {};
in {
  imports = [
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    <nixpkgs/nixos/modules/profiles/hardened.nix>
    <nixpkgs/nixos/modules/profiles/headless.nix>
  ];

  networking.hostName = "concourse-web";
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 22 443 2222 ];
  networking.interfaces.ens3.ipv6.addresses = [ { address = ipv6; prefixLength = ipv6-prefix-length; } ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    operator-public-key
  ];

  fileSystems."/" = {
    device = "/dev/sda1";
    fsType = "ext4";
  };

  fileSystems."${data-mount-point}" = {
    device = data-device;
    fsType = "ext4";
  };

  boot.cleanTmpDir = true;
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  environment.systemPackages = [ vault-scripts pkgs.vault ];

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "concourse" "vault" ];
    dataDir = "${data-mount-point}/data";
    ensureUsers = [{
      name = "concourse-web";
      ensurePermissions = { "DATABASE concourse" = "ALL PRIVILEGES"; };
    } {
      name = "vault";
      ensurePermissions = { "DATABASE vault" = "ALL PRIVILEGES"; };
    }];
  };

  # Hashicorp Vault

  services.vault = {
    enable = true;
    storageBackend = "postgresql";
    storageConfig = ''
      connection_url = "postgres:///vault?sslmode=disable&host=/run/postgresql"
    '';
    tlsCertFile = "/var/keys/vault-server-cert";
    tlsKeyFile = "/var/keys/vault-server-cert-key";
  };

  systemd.services.ensure-vault-table = {
    enable = true;
    wantedBy = [ "vault.service" ];
    before = [ "vault.service" ];
    after = [ "postgresql.service" ];
    script = ''
      set -euo pipefail

      migrate="${pkgs.haskellPackages.postgresql-simple-migration}/bin/migrate"

      CON="host=/run/postgresql dbname=vault"
      BASE_DIR="${vault-migrations}/sql"

      "$migrate" init "$CON"
      "$migrate" migrate "$CON" "$BASE_DIR"
    '';

    serviceConfig = {
      User = "vault";
      RemainAfterExit = true;
      Type = "oneshot";
    };
  };

  # Concourse web
  systemd.services.ensure-concourse-data-dir = {
    enable = true;
    wantedBy = [ "postgresql.service" ];
    before = [ "postgresql.service" ];
    script = ''
      set -euo pipefail
      mkdir -p "${data-dir}"
      chown postgres:postgres "${data-dir}"
      chmod 0750 "${data-dir}"
      echo "Created directory ${data-dir}"
    '';
    serviceConfig = {
      RemainAfterExit = true;
      Type = "oneshot";
    };
  };

  users.users.concourse-web = {
    description = "User to run concourse-web";
    extraGroups = ["keys"];
    isSystemUser = true;
  };

  users.users.vault.extraGroups = [ "keys" ];

  systemd.services.concourse-web = {
    enable = true;
    wantedBy = [ "multi-user.target" ];
    after = [ "postgresql.service" ];
    partOf = [ "postgresql.service" ];
    description = "Concourse Web";
    confinement = {
      enable = true;
      packages = [ concourse pkgs.cacert pkgs.openssl ];
    };
    script = ''
      set -euo pipefail

      cat="${pkgs.coreutils}/bin/cat"

      CONCOURSE_ENCRYPTION_KEY=$($cat /var/keys/concourse-encryption-key)

      CONCOURSE_GITLAB_CLIENT_ID=$($cat /var/keys/concourse-gitlab-client-id)
      CONCOURSE_GITLAB_CLIENT_SECRET=$($cat /var/keys/concourse-gitlab-client-secret)

      CONCOURSE_VAULT_CLIENT_TOKEN=$($cat /var/generated-keys/concourse-vault-client-token)

      export CONCOURSE_ENCRYPTION_KEY CONCOURSE_GITLAB_CLIENT_ID CONCOURSE_GITLAB_CLIENT_SECRET CONCOURSE_VAULT_CLIENT_TOKEN

      ${concourse}/bin/concourse web
    '';
    serviceConfig = {
      User = "concourse-web";
      BindReadOnlyPaths = [
        "/var/keys/concourse-session-signing-key"
        "/var/keys/concourse-tsa-host-key"
        "/var/keys/concourse-encryption-key"
        "/var/keys/concourse-tsa-authorized-keys"
        "/var/keys/concourse-gitlab-client-id"
        "/var/keys/concourse-gitlab-client-secret"
        "/var/keys/vault-ca"
        "/var/generated-keys/concourse-vault-client-token"
        "/etc/resolv.conf"
        "${config.environment.etc."ssl/certs/ca-certificates.crt".source}:/etc/ssl/certs/ca-certificates.crt"
      ];
      BindPaths = "/run/postgresql";
      CapabilityBoundingSet = "cap_net_bind_service";
      AmbientCapabilities = "cap_net_bind_service";
      NoNewPrivileges = true;
      Type = "simple";

      # Implied true by confinement.enable, but it must be false for ambient
      # capabilties to work.
      PrivateUsers = false;
    };
    environment = {
      CONCOURSE_GITLAB_HOST = "https://git.coop";
      CONCOURSE_MAIN_TEAM_GITLAB_USER = "akshay";
      CONCOURSE_POSTGRES_DATABASE = "concourse";
      CONCOURSE_POSTGRES_SOCKET = "/run/postgresql";
      CONCOURSE_SESSION_SIGNING_KEY = "/var/keys/concourse-session-signing-key";
      CONCOURSE_TSA_HOST_KEY = "/var/keys/concourse-tsa-host-key";
      CONCOURSE_TSA_AUTHORIZED_KEYS = "/var/keys/concourse-tsa-authorized-keys";
      CONCOURSE_TLS_BIND_PORT = "443";
      CONCOURSE_EXTERNAL_URL = "https://concourse.gdn";
      CONCOURSE_ENABLE_LETS_ENCRYPT = "true";
      CONCOURSE_VAULT_URL = "https://localhost:8200";
      CONCOURSE_VAULT_CA_CERT = "/var/keys/vault-ca";
      CONCOURSE_ENABLE_REDACT_SECRETS = "true";
      CONCOURSE_ENABLE_ACROSS_STEP = "true";
      CONCOURSE_ENABLE_PIPELINE_INSTANCES = "true";
      CONCOURSE_ENABLE_RESOURCE_CAUSALITY = "true";
    };
  };
}
