provider "pass" {}

data "pass_password" "operator-ssh-key" {
  path = "operator-ssh"
}

data "pass_password" "atc-session-signing-key" {
  path = "atc/session-signing-key"
}

data "pass_password" "atc-tsa-host-key" {
  path = "atc/tsa-host-key"
}

data "pass_password" "atc-encryption-key" {
  path = "atc/encryption-key"
}

data "pass_password" "atc-gitlab-client-id" {
  path = "atc/auth/git.coop/client-id"
}

data "pass_password" "atc-gitlab-client-secret" {
  path = "atc/auth/git.coop/client-secret"
}

data "pass_password" "vault-ca" {
  path = "vault/ca-cert"
}

data "pass_password" "vault-server-cert" {
  path = "vault/server-cert"
}

data "pass_password" "vault-server-cert-key" {
  path = "vault/server-cert-key"
}

data "pass_password" "vault-concourse-client-cert" {
  path = "vault/concourse-client-cert"
}

data "pass_password" "vault-concourse-client-cert-key" {
  path = "vault/concourse-client-cert-key"
}

variable "ipv6" {
  type = string
}

variable "ipv6-prefix-length" {
  type = number
  default = 64
}

variable "ipv4" {
  type = string
}

variable "data-device" {
  type = string
}

variable "workers" {
  type = set(string)
}

module "deploy_nixos" {
  source = "github.com/tweag/terraform-nixos//deploy_nixos?ref=73519ae33a6c4e03bda5d1315d36a12bc41e846e"
  build_on_target = true
  target_host = var.ipv4 # The nixos-infect script fails to set the ipv6
  ssh_private_key = data.pass_password.operator-ssh-key.full
  keys = merge ({
    concourse-session-signing-key = data.pass_password.atc-session-signing-key.full
    concourse-tsa-host-key = data.pass_password.atc-tsa-host-key.full
    concourse-tsa-authorized-keys = join("\n",[ for worker in var.workers : file("../data/public-keys/${worker}-worker-key") ])
    concourse-encryption-key = data.pass_password.atc-encryption-key.password
    concourse-gitlab-client-id = data.pass_password.atc-gitlab-client-id.password
    concourse-gitlab-client-secret = data.pass_password.atc-gitlab-client-secret.password
    vault-ca = data.pass_password.vault-ca.full
    vault-server-cert = data.pass_password.vault-server-cert.full
    vault-server-cert-key = data.pass_password.vault-server-cert-key.full
    vault-concourse-client-cert = data.pass_password.vault-concourse-client-cert.full
    vault-concourse-client-cert-key = data.pass_password.vault-concourse-client-cert-key.full
  }, {
    for f in fileset(path.module, "../data/vault-pgp-keys/*.asc"): "vault-pgp-key-${basename(f)}" => file(f)
  })
  NIX_PATH = "nixpkgs=${file("${path.module}/../result/nixpath-20.09")}"
  config = <<EOF
{ pkgs, config, ... }: (import ./configuration.nix) {
  inherit pkgs config;
  ipv6 = "${var.ipv6}";
  ipv6-prefix-length = ${var.ipv6-prefix-length};
  operator-public-key = "${file("../data/public-keys/operator.pem")}";
  data-device = "${var.data-device}";
}
EOF
}
