{stdenv}:
stdenv.mkDerivation {
  name = "vault-migrations";
  src = ./.;
  installPhase = ''
    mkdir -p $out
    cp -r sql $out/sql
  '';
}
