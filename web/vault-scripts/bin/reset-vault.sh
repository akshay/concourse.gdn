#!/usr/bin/env bash

set -euo pipefail

function log {
    echo "$@" >&2
}

export VAULT_CACERT=/var/keys/vault-ca

log "Stopping vault"
systemctl stop vault

log "Truncating vault_kv_store"
sudo -u vault psql vault -q -c 'truncate table vault_kv_store';

log "Starting vault"
systemctl start vault

log "Waiting for vault to be up"
while true; do
    set +e
    vault status >&2
    status=$?
    set -e

    # The exit code reflects the seal status:
    # - 0 - unsealed
    # - 1 - error
    # - 2 - sealed
    if [[ "$status" != "1" ]]; then
        log "Vault is up"
        break;
    fi
done

log "Initializing vault"
vault operator init \
      -pgp-keys="/var/keys/vault-pgp-key-akshay.asc" \
      -key-shares 1 \
      -key-threshold 1 \
      -format json
