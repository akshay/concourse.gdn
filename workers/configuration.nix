{ pkgs, name, config, ipv6, ipv6-prefix-length, operator-public-key, ... }:
let
  concourse = pkgs.callPackage ../nix/concourse.nix {};
  concourse-worker-dir = "/concourse-worker";
in {
  imports = [
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    <nixpkgs/nixos/modules/profiles/hardened.nix>
    <nixpkgs/nixos/modules/profiles/headless.nix>
  ];

  networking.hostName = "concourse-${name}";
  networking.firewall.allowPing = true;
  networking.interfaces.ens3.ipv6.addresses = [ { address = ipv6; prefixLength = ipv6-prefix-length; } ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    operator-public-key
  ];

  fileSystems."/" = {
    device = "/dev/sda1";
    fsType = "ext4";
  };

  boot.cleanTmpDir = true;
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  # These need to be added because the nixos hardened module disables any
  # additional kernel module addition.
  #
  # For changes to this list to be applied, a reboot is necessary.
  boot.kernelModules = [ "overlay" "veth" "xt_comment" "xt_MASQUERADE" "ipt_REJECT" ];

  systemd.services.concourse-worker = {
    enable = true;
    wantedBy = [ "multi-user.target" ];
    description = "Concourse Worker";
    script = ''
      set -euo pipefail

      mkdir -p ${concourse-worker-dir}
      ${concourse}/bin/concourse worker
    '';
    path = [ pkgs.iptables pkgs.gnutar pkgs.gzip ];
    environment = {
      CONCOURSE_WORK_DIR = concourse-worker-dir;
      CONCOURSE_TSA_HOST = "concourse.gdn:2222";
      CONCOURSE_TSA_PUBLIC_KEY = "/var/keys/concourse-tsa-public-key";
      CONCOURSE_TSA_WORKER_PRIVATE_KEY = "/var/keys/concourse-worker-key";
      CONCOURSE_RUNTIME = "containerd";
      CONCOURSE_CONTAINERD_INIT_BIN = "${concourse}/bin/init";
      CONCOURSE_CONTAINERD_CNI_PLUGINS_DIR = "${concourse}/bin";
    };
  };
}
