provider "pass" {}

data "pass_password" "operator-ssh-key" {
  path = "operator-ssh"
}

variable "workers" {
  type = map(object({
    ipv4_address = string,
    ipv6_address = string
  }))
}

data "pass_password" "worker-key" {
  for_each = var.workers
  path     = "${each.key}/worker-key"
}

variable "ipv6-prefix-length" {
  type    = number
  default = 64
}


module "deploy_nixos" {
  for_each = var.workers

  source          = "github.com/tweag/terraform-nixos//deploy_nixos?ref=73519ae33a6c4e03bda5d1315d36a12bc41e846e"
  build_on_target = true
  target_host     = each.value.ipv4_address # The nixos-infect script fails to set the ipv6
  ssh_private_key = data.pass_password.operator-ssh-key.full
  keys = {
    concourse-tsa-public-key = file("../data/public-keys/tsa-host-key")
    concourse-worker-key     = data.pass_password.worker-key[each.key].full
  }
  NIX_PATH = "nixpkgs=${file("${path.module}/../result/nixpath-20.09")}"
  config   = <<EOF
{ pkgs, config, ... }: (import ./configuration.nix) {
  inherit pkgs config;
  name = "${each.key}";
  ipv6 = "${each.value.ipv6_address}";
  ipv6-prefix-length = ${var.ipv6-prefix-length};
  operator-public-key = "${file("../data/public-keys/operator.pem")}";
}
EOF
}
