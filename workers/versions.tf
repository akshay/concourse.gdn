terraform {
  required_providers {
    pass = {
      source = "camptocamp/pass"
    }
  }
  required_version = ">= 0.14"
}
